#!/bin/sh
CA_CERT=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
curl -k -X PUT --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" -d @icc.json "https://137.226.232.175:6443/apis/apps/v1/namespaces/myedulife/icc"
POD_NAME=$(curl -k --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" "https://137.226.232.175:6443/api/v1/namespaces/myedulife/pods" | jq '.items[] | .metadata.name' | grep -v 'myedulife-icc' | grep -m 1 'icc' | sed 's/"//g')
curl -k -X DELETE --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" "https://137.226.232.175:6443/api/v1/namespaces/myedulife/pods/$POD_NAME"
