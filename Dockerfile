FROM node:18-alpine as build
#FROM trion/ng-cli-e2e

USER root

ENV BUILD_DEPS="gettext"  \
    RUNTIME_DEPS="libintl"

RUN set -x && \
    apk add --update $RUNTIME_DEPS && \
    apk add --virtual build_deps $BUILD_DEPS &&  \
    cp /usr/bin/envsubst /usr/local/bin/envsubst && \
    apk del build_deps

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm install -f

# build app
COPY . /app
RUN npm install -g @angular/cli
RUN ng build --configuration=production

FROM nginx:latest

COPY --from=build app/dist/de.tud.codip.issuer-control-center /usr/share/nginx/html
COPY nginx/default.conf /etc/nginx/conf.d/
RUN envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js

EXPOSE 4200

# serve app
#CMD envsubst < dist/de.tud.codip.issuer-control-center/assets/env.template.js > dist/de.tud.codip.issuer-control-center/assets/env.js && ng serve --host 0.0.0.0 --port 4200 --disable-host-check --configuration=production
