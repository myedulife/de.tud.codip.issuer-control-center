export const environment = {
  production: true,
  // @ts-ignore
  keycloak: window["env"]["kcUrl"] || "https://kc.myedulife.dbis.rwth-aachen.de",
  iccservice: "https://iccservice.myedulife.dbis.rwth-aachen.de",
  version: require("../../package.json").version
};
