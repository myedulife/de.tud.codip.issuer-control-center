import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IssuerService } from '../services/issuer.service';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';

export interface PeriodicElement {
  position: number;
  statusCode: string;
  familyName: string;
  givenName: string;
  courseTitle: string;
}

@Component({
  selector: 'app-certificate-upload',
  templateUrl: './certificate-upload.component.html',
  styleUrls: ['./certificate-upload.component.scss'],
  animations: [
      trigger('detailExpand', [
        state('collapsed,void', style({height: '0px', minHeight: '0'})),
        state('expanded', style({height: '*'})),
        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      ]),
  ]
})
export class CertificateUploadComponent implements OnInit {

  @Input()  withFinishStep = true;

  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  message = '';
  messageType = 'danger';
  isResult = false;
  results:any;
  tableRowCssClass = "tableRowDefault";

  columnsToDisplay: string[] = ['select', 'statusCode', 'familyName', 'givenName', 'courseTitle'];
  columnsToDisplayWithExpand: string[] = [...this.columnsToDisplay, 'expand'];
  dataSource = new MatTableDataSource<PeriodicElement>();
  selection = new SelectionModel<PeriodicElement>(true, []);
  expandedElement: PeriodicElement | null;

  firstFormGroup: FormGroup = this._formBuilder.group({firstCtrl: ['']});
  secondFormGroup: FormGroup = this._formBuilder.group({secondCtrl: ['']});

  @ViewChild('stepper') private myStepper: MatStepper;
  totalStepsCount: number;

  constructor(private issuerService: IssuerService, private _formBuilder: FormBuilder) { }

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
  }

  ngAfterViewInit() {
    this.totalStepsCount = this.myStepper._steps.length;
  }

  upload(): void {
    this.progress = 0;
    this.isResult = false;
    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);
      if (file) {
        this.currentFile = file;
        this.issuerService.parseTemplate(this.currentFile).subscribe(
          (event: any) => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round(100 * event.loaded / event.total);
            } else if (event instanceof HttpResponse) {
              this.message = "Die Daten wurden erfolgreich verarbeitet. Die Nachweise können jetzt erstellt und versendet werden.";
              this.messageType = "success";
              this.results = this.addPositionCol(event.body.results);
              if (this.results.length > 0) {
                this.isResult = true;
                this.dataSource = new MatTableDataSource<PeriodicElement>(this.results);
                this.myStepper.next();
              }
            }
          },
          (err: any) => {
            console.log(err);
            this.progress = 0;
            this.messageType = "danger";
            if (err.error && err.error.message) {
              this.message = err.error.message;
            } else {
              this.message = 'Upps ... da ging etwas schief. Leider konnten keine digitalen Zertifikate erstellt werden. Bitte kontaktieren Sie die verantwortliche Person.';
            }
            this.currentFile = undefined;
          });
      }
      this.selectedFiles = undefined;
    }
  }

  addPositionCol(map:any[]) {
    let newMap:any[] = [];
    let counter = 1;
    map.forEach(function (value) {
      value['position'] = counter
      counter++;
      newMap.push(value);
    });
    return newMap;
  }

  ngOnInit(): void {

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  backToUpload() {
    this.isResult = false;
    this.message = "";
    this.selectedFiles = undefined;
    this.currentFile = undefined;
    this.progress = 0;
  }

  createCertificates() {
    this.issuerService.generateVerifiableCredential(this.selection["_selected"]).subscribe(genericCertificates => {
      this.selection.clear();
      let newResults:any[] = [];
      this.results.forEach(function (result:any) {
        let newResult = result;
        genericCertificates.results.forEach( function (cert:any) {
          if (cert.position === result.position) {
            newResult = cert;
            // I need a break
          }
        });
        newResults.push(newResult);
      });
      console.log("New results: " + JSON.stringify(newResults));
      this.dataSource = new MatTableDataSource<PeriodicElement>(newResults);
    });
  }

  getTableRowCssClass(element:any):string {
    let cssClass = "tableRowDefault";
    if (element.status == "Versendet") {
      cssClass = "tableRowSent";
    }
    if (!this.checkMailValidity(element.mail)) {
      cssClass = "tableRowWarning";
    }
    return cssClass;
  }

  checkMailValidity(mail:string) {
    return String(mail)
               .toLowerCase()
               .match(
                 /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
               );
  }

}

