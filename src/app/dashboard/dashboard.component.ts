import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public isLoggedIn = false;
  public isAdmin = false;
  public userProfile: KeycloakProfile;

  constructor(private keycloakService: KeycloakService) { }

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloakService.isLoggedIn();
    if (this.isLoggedIn) {
      this.userProfile = await this.keycloakService.loadUserProfile();
    }
    this.isAdmin = this.keycloakService.isUserInRole("admin");
  }

  logout(): void {
    this.keycloakService.logout(window.location.origin);
  }

}
