import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { UserViewComponent } from './user-view/user-view.component';
import { PermissionDeniedComponent } from './permission-denied/permission-denied.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { CertificateTemplateComponent } from './certificate-template/certificate-template.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { OnboardingViewComponent } from './onboarding-view/onboarding-view.component';
import { ImpressumViewComponent } from './impressum-view/impressum-view.component';
import { VcViewComponent } from './vc-view/vc-view.component';
import { environment } from '../environments/environment';
import { AlertModule } from '@coreui/angular';
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";
import { sanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { CertificateUploadComponent } from './certificate-upload/certificate-upload.component';
import { MatStepperModule } from '@angular/material/stepper';
import { NgHcaptchaModule } from 'ng-hcaptcha';

function initializeKeycloak (keycloak: KeycloakService) {
    return () =>
      keycloak.init({
        config: {
          url: environment.keycloak,
          realm: 'control-center',
          clientId: 'angular-js-app-client',
        },
        initOptions: {
          onLoad: 'check-sso',
          silentCheckSsoRedirectUri:
            window.location.origin + '/assets/silent-check-sso.html',
        },
        loadUserProfileAtStartUp: true
      });
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AdminViewComponent,
    UserViewComponent,
    PermissionDeniedComponent,
    CertificateTemplateComponent,
    FileUploadComponent,
    HomeViewComponent,
    OnboardingViewComponent,
    ImpressumViewComponent,
    VcViewComponent,
    sanitizeHtmlPipe,
    CertificateUploadComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    KeycloakAngularModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    HttpClientModule,
    MatStepperModule,
    AlertModule,
    NgHcaptchaModule.forRoot({
        siteKey: '6218ca81-033c-4931-843e-fe7df7c24ad6'
    }),
    MatPasswordStrengthModule.forRoot()
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
