import { Component, OnInit } from '@angular/core';
import { RegisterModel } from '../auth/register.model';
import { AuthenticationService } from '../services/authentication.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-onboarding-view',
  templateUrl: './onboarding-view.component.html',
  styleUrls: ['./onboarding-view.component.scss']
})
export class OnboardingViewComponent implements OnInit {
  barLabel: string = "Passwortstärke:";
  model = new RegisterModel("", "", "", "", "", "", "", false);
  submitted = false;
  message = '';
  messageType = 'danger';
  captcha = false;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.submitted = true;
    this.authenticationService.signup(this.model).subscribe(
      (event: any) => {
        if (event instanceof HttpResponse) {
          //this.message = event.body.message;
          console.log(event.body.message);
        } else {
          //this.message = event.message;
          console.log(event.message);
        }
        this.message = "Sie haben sich erfolgreich registriert. Vielen Dank!"
        this.messageType = "success";
      },
      (err: any) => {
        if (err.error && err.error.message) {
          this.message = err.error.message;
        } else {
          this.message = 'Unknown error';
        }
        this.messageType = "danger";
      });
  }

  onVerify(token: string) {
      console.log("verify");
      this.captcha = true
  }

  onExpired(response: any) {
      console.log("expired");
  }

  onError(error: any) {
      console.log("error");
  }

}
