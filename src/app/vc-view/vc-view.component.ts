import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { CryptoService } from '../services/crypto.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-vc-view',
  templateUrl: './vc-view.component.html',
  styleUrls: ['./vc-view.component.scss']
})
export class VcViewComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;
  fileAttr = 'Wähle Datei';
  vc:any;
  form: FormGroup;
  visual:SafeHtml;
  canParse:boolean = false;
  version:string;
  validateEnabled = false;
  validPubKey = false;
  validationClass = "certnotvalid";

  constructor(public fb: FormBuilder, private sanitizer: DomSanitizer, private cryptoService: CryptoService) {
    this.form = this.fb.group({
      name: [''],
      vc: [null]
    })
    this.visual = this.sanitizer.bypassSecurityTrustHtml("");
  }

  ngOnInit(): void {
    this.version = environment.version;
  }

  uploadFileEvt(jsonFile: any) {
      if (jsonFile.target.files && jsonFile.target.files[0]) {
        this.fileAttr = '';
        Array.from(jsonFile.target.files).forEach((file: any) => {
          this.fileAttr += file.name + ' - ';
        });
        // HTML5 FileReader API
        let reader = new FileReader();
        reader.readAsText(jsonFile.target.files[0], "UTF-8");
        reader.onload = (e: any) => {
          this.vc = JSON.parse(e.target.result as string);
          this.visual = this.sanitizer.bypassSecurityTrustHtml(this.vc.credentialSubject.visualRepresentation);
          this.cryptoService.verifyPublicKey(this.vc.issuerPublicKey).subscribe(
                                          (event: boolean) => {
                                              //console.log("CryptoService1: " + event);
                                              this.validPubKey = event;
                                              if (this.validPubKey) {
                                                this.validationClass = "certpartlyvalid";
                                              }
                                          },
                                          (err: any) => {
                                            console.log("Cannot verify public key. Event: " + event);
                                          });
          this.canParse = true;
        };
        reader.readAsDataURL(jsonFile.target.files[0]);
        // Reset if duplicate image uploaded again
        this.fileInput.nativeElement.value = '';
      } else {
        this.canParse = false;
        this.fileAttr = 'Wähle Datei';
      }
    }

}
