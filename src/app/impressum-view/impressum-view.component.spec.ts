import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpressumViewComponent } from './impressum-view.component';

describe('ImpressumViewComponent', () => {
  let component: ImpressumViewComponent;
  let fixture: ComponentFixture<ImpressumViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImpressumViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImpressumViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
