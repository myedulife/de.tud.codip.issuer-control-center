import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { UserViewComponent } from './user-view/user-view.component';
import { ImpressumViewComponent } from './impressum-view/impressum-view.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { OnboardingViewComponent } from './onboarding-view/onboarding-view.component';
import { VcViewComponent } from './vc-view/vc-view.component';
import { PermissionDeniedComponent } from './permission-denied/permission-denied.component';
import { AppAuthGuard } from './auth/app.auth.guard';
import { CertificateTemplateComponent } from './certificate-template/certificate-template.component';
import { EscoComponent } from './esco/esco.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AppAuthGuard],
    children: [
      {
        path: '',
        component: HomeViewComponent,
        canActivate: [AppAuthGuard]
      },
      {
        path: 'admin',
        component: AdminViewComponent,
        data: {
          roles: ['admin']
        },
        canActivate: [AppAuthGuard]
      },
      {
        path: 'admin/certificate-template',
        component: CertificateTemplateComponent,
        data: {
          roles: ['admin']
        },
        canActivate: [AppAuthGuard]
      },
      {
        path: 'admin/esco',
        component: EscoComponent,
        data: {
          roles: ['admin']
        },
        canActivate: [AppAuthGuard]
      },
      {
        path: 'user',
        component: UserViewComponent,
        canActivate: [AppAuthGuard]
      },
      {
        path: 'permission-denied',
        component: PermissionDeniedComponent
      }
    ]
  },
  {
    path: 'onboarding',
    component: OnboardingViewComponent
  },
  {
    path: 'vc',
    component: VcViewComponent
  },
  {
    path: 'impressum',
    component: ImpressumViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
