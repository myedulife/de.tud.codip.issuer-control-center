import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

import {MatTableModule} from '@angular/material/table';

export interface PeriodicElement {
  position: string;
  key: string;
  value: string | undefined;
}

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  public isLoggedIn = false;
  public isAdmin = false;
  public userProfile: KeycloakProfile;

  displayedColumns: string[] = ['position', 'key', 'value'];
  dataSource:PeriodicElement[] = [];

  constructor(private keycloakService: KeycloakService) { }

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloakService.isLoggedIn();
    if (this.isLoggedIn) {
      this.userProfile = await this.keycloakService.loadUserProfile();
    }
    this.isAdmin = this.keycloakService.isUserInRole("admin");
    this.dataSource = [];
    // username
    let entryUsername:PeriodicElement = {
      position: "1",
      key: "Benutzername",
      value: this.userProfile.username
    };
    this.dataSource.push(entryUsername);
    // firstname
    let entryFirstname:PeriodicElement = {
      position: "2",
      key: "Vorname",
      value: this.userProfile.firstName
    };
    this.dataSource.push(entryFirstname);
    // username
    let entryLastname:PeriodicElement = {
      position: "3",
      key: "Nachname",
      value: this.userProfile.lastName
    };
    this.dataSource.push(entryLastname);
    // email
    let entryEmail:PeriodicElement = {
      position: "4",
      key: "E-Mail",
      value: this.userProfile.email
    };
    this.dataSource.push(entryEmail);
  }

}
