import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  private baseUrl = environment.iccservice + '/crypto';

  constructor(private http: HttpClient) { }

  //public async verify( value: string, key: CryptoKey, params: CryptoSigningParams, signature: ArrayBuffer ): Promise<boolean> {
  //    const encodedString: Uint8Array = this.encode(value);
  //    return await crypto.subtle.verify(params, key, signature, encodedString);
  //}

  public verifyPublicKey(key:string): Observable<boolean> {
    return this.http.get<boolean>(this.baseUrl + "/verifypk/" + key)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
      if (error.status === 0) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong.
        console.error(
          `Backend returned code ${error.status}, body was: `, error.error);
      }
      // Return an observable with a user-facing error message.
      return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
