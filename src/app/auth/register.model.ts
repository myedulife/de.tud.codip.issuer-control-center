export class RegisterModel {
  constructor(
    public nickname: string,
    public password: string,
    public firstName: string,
    public lastName: string,
    public confirmPassword: string,
    public email: string,
    public walletUrl: string,
    public privacyAndTermsOfUse: boolean,
  ) {}
}
