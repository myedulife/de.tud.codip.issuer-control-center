# Installation

## Quick Setup for Developers

In the first step you clone the project and install all dependencies and tools.
```shell
cd projects # go to your projects folder on your system 
git clone git@gitlab.com:myedulife/de.tud.codip.issuer-control-center.git
cd de.tud.codip.issuer-control-center
npm install # install the dependencies
sudo npm install -g @angular/cli # install the angular cli on your system (system wide)
```

In our project we work with Git Flow. Therefore it is important that you initialize Git Flow [[1]](https://danielkummer.github.io/git-flow-cheatsheet/index.html) in your local project. In summary, we don't do checkins on the master branch, but only on develop.
```shell
sudo apt-get install git-flow
git flow init # Use the default settings. The productive branch is 'master'.
```
